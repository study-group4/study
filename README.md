Our HAT works like a VU meter, turning on LED's as the volume from our input signal goes up. The higher the volume, the more LED's are on.

#Systems
#Subsystem 1
This subsystem supplies the voltage the system needs. It feeds voltage to subsystem 2 and subsystem 3 and steps the voltage up to 5 V using a buck convertor

#Subsystem 2
This subsystem 2 is a filte module, it filters the unwanted frequencies out to feed it back into subsystem 3.

#Subsystem 3
This subsystem is the visual part of the system. It recieves the voltage from subsystem 1 and the signal from subssytem 2 and outputs it by showing LED's go on as the signal(volume) increases.
